import os
from typing import Optional

fileFormat = "bze";

__all__ = ['pathValid','getFileByteSize']

def pathValid(pth:str)->bool:
    ret=False;
    fileFormat_clean = fileFormat.lower().replace(".","");

    ret = os.path.exists(pth) and pth.lower().endswith("."+fileFormat_clean);
    return ret;

def getFileByteSize(pth:str)->Optional[int]:
    ret = None;

    if(pathValid(pth)):
        ret = os.path.getsize(pth)

    return ret;
