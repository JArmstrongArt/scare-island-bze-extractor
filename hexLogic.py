from typing import Optional
from typing import List
import pathValidator
import genericMaths
byteCharCount = 2;
hexChars = 'abcdef1234567890';
hexCharBase = len(hexChars);


__all__ = ['readBytesFromFile','byteStringIntoSingleByte','formatByteStringToHexString']







#this function only works if we're doing hexadecimal for sure, since the rest of the script is set up to handle any base, not just base 16
#also, .hex() assumes big endian, but bze is little endian, so it has to be flipped
def formatByteStringToHexString(byteStr:bytes, flipEndian:bool=True)->Optional[str]:
    ret=None;
    bigEndianRet = byteStr.hex();
    if(flipEndian==False):
        ret = bigEndianRet;
    else:
        bytesInString= []
        currentByte="";
        for x in range(0,len(bigEndianRet)):
            if(x%byteCharCount==0):
                if(currentByte.replace(" ","")!=""):
                    bytesInString.append(currentByte);
                currentByte = "";
            currentByte+=bigEndianRet[x];


        if(len(bytesInString)>0):
            bytesInString.reverse();
            ret="";
            for x in bytesInString:
                ret+=x;

    return ret;
            




def addTwoHexStrings(strA:str, strB:str)->Optional[str]:
    ret = None;
    strA_asHex = stripStringToHexChars(strA);
    strB_asHex = stripStringToHexChars(strB);

    if(strA_asHex!=None and strB_asHex!=None):
        intA = int(strA_asHex,hexCharBase);
        intB = int(strB_asHex,hexCharBase);

        intC = intA + intB;
        ret = str(hex(intC));
        while "0x" in ret:
            ret = ret.replace("0x","");

    return ret;
        



def formatStringToByteString(baseString:str)->Optional[bytes]:
    ret = None;

    baseString_asHex = stripStringToHexChars(baseString);

    if(baseString_asHex!=None):
        formattingIndex=len(baseString_asHex);


        while formattingIndex >0:
            formattingIndex-=byteCharCount;
            beginStr = baseString_asHex[:formattingIndex];
            endStr = baseString_asHex[formattingIndex:];
            baseString_asHex = beginStr+"\\x"+endStr;
            

            
        ret = bytes(baseString_asHex.encode().decode('unicode_escape').encode("raw_unicode_escape"));

    return ret;


def byteStringIntoSingleByte(byteString:bytes,byteIndex:int)->Optional[bytes]:
    ret = None;

    if(len(byteString)>0):
        if(byteIndex>=0 and byteIndex<len(byteString)):
            ret = byteString[byteIndex:byteIndex+1];
    
    return ret;




def stripStringToHexChars(string:str)->Optional[str]:
    ret = None;
        
    if(len(string.replace(" ",""))>0):
        for x in string:
            if x.lower() in hexChars.lower():
                if(ret==None):
                    ret="";
                ret+=x;


    if(ret!=None):
        while len(ret)%byteCharCount!=0:
            ret="0"+ret;

        
    return ret;



def hexStringToInteger(string:str)->int:
    return int(string.upper(), hexCharBase);

def readBytesFromFile(pth:str, byteOffset_asHex:str,byteCount:int)->Optional[bytes]:

    ret_asByteString= None;

    
    byteOffset_asHex = stripStringToHexChars(byteOffset_asHex);

    if(byteOffset_asHex!=None):

        if(pathValidator.pathValid(pth)):
            fileByteCount = pathValidator.getFileByteSize(pth);
            file = open(pth,'rb');


            
            byteOffset_asHex_cleaned = "";
            for ch in byteOffset_asHex:
                if ch.lower() in hexChars.lower():
                    byteOffset_asHex_cleaned += ch.upper();



            byteOffset_int = hexStringToInteger(byteOffset_asHex_cleaned);


            if(byteOffset_int<fileByteCount):
                file.seek(byteOffset_int);


                byteCount_maxReadable = fileByteCount-byteOffset_int;
                
                byteCount = genericMaths.intClamp(byteCount,1,byteCount_maxReadable);
                ret_asByteString = file.read(byteCount);


            file.close();

    return ret_asByteString;
