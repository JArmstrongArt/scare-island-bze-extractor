__all__ = ['intClamp','clamp'];

def clamp(n:float, smallest:float, largest:float)->float:
    if(n<smallest):
        n=smallest;

    if(n>largest):
        n=largest;

    return n;

def intClamp(n:float, smallest:float, largest:float)->int:
    return int(round(clamp(n,smallest,largest)));
