import os

import hexLogic
import pathValidator
import genericMaths

internalFiles_fileList_readType = genericMaths.intClamp(1,1,2); #1 = dont include 0s padding, 2 = endload 0s padding

def verifyMonstersIncBZE(pth:str):
    ret=False;

    pthValid = pathValidator.pathValid(pth);

    if(pthValid):
        checkList = hexLogic.readBytesFromFile(pth,"0",4);
        ret=True;

        for x in range(0,len(checkList)):
            singleByteCheck = hexLogic.byteStringIntoSingleByte(checkList,x);
            if((singleByteCheck!=None and singleByteCheck!=hexLogic.formatStringToByteString('00')) or singleByteCheck==None):
                ret=False;
                break;

    return ret;
    
def processFile(pth:str,validFileCheck:bool):


    if(validFileCheck):
        internalFiles_count_asByteString = hexLogic.readBytesFromFile(pth,"8",4);

        if(internalFiles_count_asByteString!=None):

            internalFiles_count = int.from_bytes(internalFiles_count_asByteString, "little");

            internalFiles_fileListData = []; #arrays within arrays. [0] = file type (currently unknown use), [1] = file size in bytes, [2] file size in bytes rounded up to some weird value to pad out data

            internalFiles_fileList_readHead = 'c';#start of file list
            for x in range(0,internalFiles_count):
                internalFiles_fileListData_type = hexLogic.readBytesFromFile(pth,internalFiles_fileList_readHead,4);
                internalFiles_fileListData_size = hexLogic.readBytesFromFile(pth,hexLogic.addTwoHexStrings(internalFiles_fileList_readHead,"4"),4);
                internalFiles_fileListData_sizeRnd = hexLogic.readBytesFromFile(pth,hexLogic.addTwoHexStrings(internalFiles_fileList_readHead,"8"),4);
                internalFiles_fileListData.append([internalFiles_fileListData_type,internalFiles_fileListData_size,internalFiles_fileListData_sizeRnd]);
                internalFiles_fileList_readHead = hexLogic.addTwoHexStrings(internalFiles_fileList_readHead,"c");


            internalFiles_fileList_readHead = '800';#start of data

            
            for x in range(0,len(internalFiles_fileListData)):
                d=None;



                x_size = hexLogic.hexStringToInteger(hexLogic.formatByteStringToHexString(internalFiles_fileListData[x][internalFiles_fileList_readType]));
                
                if(x_size!=None):

                    d = hexLogic.readBytesFromFile(  pth  ,  internalFiles_fileList_readHead  , x_size   );

                x_readHead = hexLogic.addTwoHexStrings(  internalFiles_fileList_readHead  ,  hexLogic.formatByteStringToHexString(  internalFiles_fileListData[x][2]  )  );

                if(x_readHead!=None):
                    internalFiles_fileList_readHead  =  x_readHead;

                if(d!=None):

                    x_writeDest = open("file_"+str(x),'wb');
                    x_writeDest.write(d);
                    x_writeDest.close()

                            

                        


                        


def main():

    fPath = "";

    fileValid=False;
    
    while fileValid==False:
        fPath = str(input("Input the path to a 'Monsters Inc. Scare Island' .BZE file here: ")).replace('"','');

        if(not pathValidator.pathValid(fPath)):
            print("file "+fPath+ " doesn't exist or is otherwise invalid!");
        else:

            if(verifyMonstersIncBZE(fPath)):
                fileValid=True;
            else:
                print("The file is a BZE file, but not a 'Monsters Inc. Scare Island' .BZE file.");

    processFile(fPath,fileValid);


            


main();


